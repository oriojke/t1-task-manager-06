package ru.t1.didyk.taskmanager.constant;

public class ArgumentConst {

    public static final String HELP = "-h";

    public static final String VERSION = "-v";

    public static final String ABOUT = "-a";

    private ArgumentConst() {
    }
}
